---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
background: /assets/img/peak-5645235_1920.jpg
---

# Willkommen auf meiner Webseite
Hi. Cool, dass du hier gelandet bist. Auf dieser Webseite kannst du mich etwas kennenlernen, sieh dich gerne etwas um! Ach ja, ich schreibe in sehr unregelmäßigen Abständen mal Artikel in meinen Blog, vielleicht ist ja was Interessantes für dich dabei!
<hr>
## Letzte Beiträge
